# Simplified Template Cross Section Measurement
# for gluon fusion. Contains bbH. Strictly speaking, should also
# contain gg->Zh->(jj)h contribution, but neglected here.
# Data taken from Sec. 8.2.4, ATLAS-CONF-2017-045
# Measured XS is given in pb
201704503
ATLAS-CONF-2017-045
LHC, ATL, ATL
(pp)->h->gamma gamma (ggH1j medium)
13	36.1	0.032
0	0
125.09
2.5
1	125.09
11
1.0
-0.001	0.005	0.011
0.004	0.010	0.017