# Simplified Template Cross Section Measurement
# for ttH production. Contains also tH contribution,
# but neglected here.
# Data taken from Sec. 8.2.4, ATLAS-CONF-2017-045.
# Measured XS is given in pb
201704509
ATLAS-CONF-2017-045
LHC, ATL, ATL
(pp)->h->gamma gamma (top)
13	36.1	0.032
0	0
125.09
2.5
1   125.09
51
1.0
0.0000	0.0007	0.0015
0.0005	0.0013	0.0022