# Published in the ILC Technical Design Report 2013 - Volume 2
# Tab. 2.4, updated numbers from arXiv:1310.0763, Tab.5.4.
# Polarization: (e-,e+)=(-0.8,0.3)
500111	50011	1
arXiv:1306.6352
ILC, ILC, ILC
(ee)->WBF->WW
500	500	0.001
1	0
0.2
125.0 125.0 0.1
1	-1
32

125.0	0.966	1.000	1.034
