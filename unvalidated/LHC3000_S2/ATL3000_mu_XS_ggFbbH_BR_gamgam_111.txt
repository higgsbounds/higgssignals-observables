# Projection to LHC 3000fb-1.
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/GuidelinesCouplingProjections2018
111 111 1
HL/HE-LHC_YR2018
LHC, ATL, ATL
ggFbbH,H->gamgam
13 3000 0.01
1 0
10.0
125.0 125.0 1.0
1 125.0
1.1
1.0
125 0.94932 1.0 1.05068
