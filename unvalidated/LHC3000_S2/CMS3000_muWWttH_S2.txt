# Projection to LHC 3000fb-1.
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/GuidelinesCouplingProjections2018
43 43 1
HL/HE-LHC_YR2018
LHC, CMS, CMS
ttH,H->WW
13 3000 0.01
1 0
10.0
125.0 125.0 1.0
1 125.0
5.2
1.0
125 0.903 1.0 1.097
