# Projection to LHC 3000fb-1.
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/GuidelinesCouplingProjections2018
9 9 1
HL/HE-LHC_YR2018
LHC, CMS, CMS
ggH,H->bb
13 3000 0.01
1 0
10.0
125.0 125.0 1.0
1 125.0
1.5
1.0
125 0.753 1.0 1.247
