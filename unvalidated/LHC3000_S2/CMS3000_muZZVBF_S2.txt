# Projection to LHC 3000fb-1.
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/GuidelinesCouplingProjections2018
15 15 1
HL/HE-LHC_YR2018
LHC, CMS, CMS
VBF,H->ZZ
13 3000 0.01
1 0
10.0
125.0 125.0 1.0
1 125.0
2.3
1.0
125 0.866 1.0 1.134
