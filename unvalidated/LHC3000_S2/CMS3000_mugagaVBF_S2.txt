# Projection to LHC 3000fb-1.
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/GuidelinesCouplingProjections2018
13 13 1
HL/HE-LHC_YR2018
LHC, CMS, CMS
VBF,H->gaga
13 3000 0.01
1 0
10.0
125.0 125.0 1.0
1 125.0
2.1
1.0
125 0.872 1.0 1.128
