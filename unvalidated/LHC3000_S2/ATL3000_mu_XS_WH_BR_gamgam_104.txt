# Projection to LHC 3000fb-1.
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/GuidelinesCouplingProjections2018
104 104 1
HL/HE-LHC_YR2018
LHC, ATL, ATL
WH,H->gamgam
13 3000 0.01
1 0
10.0
125.0 125.0 1.0
1 125.0
3.1
1.0
125 0.85789 1.0 1.14211
