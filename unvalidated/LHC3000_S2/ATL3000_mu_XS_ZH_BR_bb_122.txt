# Projection to LHC 3000fb-1.
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/GuidelinesCouplingProjections2018
122 122 1
HL/HE-LHC_YR2018
LHC, ATL, ATL
ZH,H->_bb
13 3000 0.01
1 0
10.0
125.0 125.0 1.0
1 125.0
4.5
1.0
125 0.9415 1.0 1.0585
